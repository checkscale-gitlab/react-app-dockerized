FROM node:alpine3.13 as alpine

RUN mkdir /app

WORKDIR /app

COPY package.json /app

COPY package-lock.json /app

RUN npm ci --only=production

COPY . /app

RUN npm run build


FROM nginx:1.19.9-alpine

COPY --from=alpine /app/build /usr/share/nginx/html